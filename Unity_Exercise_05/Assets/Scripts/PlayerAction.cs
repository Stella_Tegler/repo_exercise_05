﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class PlayerAction : MonoBehaviour
    {
        public PlayerState playerState;
        private float movementSpeed;

        // Update is called once per frame
        void Update()
        {
            switch (playerState)
            {
                case PlayerState.NONE:
                    movementSpeed = 0.0f;
                    Debug.Log(message: "Boi sleeping at 0km/h");
                    break;

                case PlayerState.IDlE:
                    movementSpeed = 2.5f;
                    Debug.Log(message: "Boi getting up at 2.5km/h");
                    break;

                case PlayerState.WALKING:
                    movementSpeed = 10.8f;
                    Debug.Log(message: "Boi on a walk at 10.8km/h");
                    break;

                case PlayerState.RUNNING:
                    movementSpeed = 35.9f;
                    Debug.Log(message: "Fast boi at 35.9km/h");
                    break;
            }
        }
    }
}