﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Health : MonoBehaviour
    {
        public bool isAlive;
        public int health = 100;

        // Update is called once per frame
        void Update()
        {
            if (isAlive)
            {
                Debug.Log(message: "Player is alive");

                if (health <= 10)
                {
                    Debug.Log(message: "Player is almost dead");
                }
            }
        }
    }
}